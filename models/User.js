/*
	user schema:
	REQUIRED FIELDS
	firstName - string
	lastName - string
	age - string
	gender - string
	email - string
	password - string
	mobileNo - string
	
	NOT REQUIRED BUT HAS DEFAULT
	isAdmin - Boolean, false

	enrollment array - will show the courses (id) where the students are enrolled and the date of their enrollment

	push the updates on your s32-36 git repo with the commit message "add user schema"
	add it on Boodle (WDC028 Express.js - API Development Part 1)
*/

const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
	firstName:{
		type: String,
		required: [true, "First name is required"]
	},
	lastName:{
		type: String,
		required:[true,"Last name is required"]
	},
	age:{
		type: String,
		required: [true,"Age is required"]
	},
	gender:{
		type: String,
		required: [true,"Gender is required"]
	},
	email:{
		type: String,
		required: [true,"eMail is required"]
	},
	password:{
		type: String,
		required: [true,"Password is required"]
	},
	mobileNo: {
		type: String,
		required: [true,"Mobile number is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	enrollment:[
	{
		courseId:{
			type: ObjectId,
			required: [true,"Please add course"]
		},
		createdOn:{
			type: Date,
			default: new Date()
		}
	}
	] 
})